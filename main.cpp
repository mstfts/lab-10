#include <iostream>
#include <string>
#include <memory>

class Entity
{
private:
    std::string name;

public:
    const std::string &getName() const { return name; }
    std::string &getName() { return name; }

    Entity(const std::string &name) : name(name)
    {
        std::cout << " Объект "<< name <<" создан! " << std::endl;
    };

    Entity() : name("Unknown")
    {
        std::cout << " Объект "<< name <<" создан! " << std::endl;
    };

    ~Entity()
    {
        std::cout << " Объект "<< name <<" удалён! " << std::endl;
    };
};

int main(int argc, char *argv[])
{
    void *p1 = nullptr;

    std::cout << " Нулевой указатель = " << p1 << std::endl;

    int a = 123;
    int *a_ptr = &a;
    std::cout << "a = " << a << std::endl;

    std::cout << " Указатель a_ptr ("<< a_ptr <<") со значением " << *a_ptr << std::endl;

    int *b = new int[8];
    for (int i = 0; i < 8; i++)
        b[i] = i;
    std::cout << " Указатель на массив ("<< b <<") со значением " << *b << std::endl;

    int *b_init = b;
    std::cout << " Увеличенный указатель на массив ("<< ++ b <<") со значением " << *b << std::endl;
    std::cout << " Разница = " << (b - b_init) * sizeof(int) << "байт" << std::endl;

    b = b_init;
    std::cout << " Доказательство того, что a [i] = i [a] = * (a + i) " << std::endl;
    std::cout << "b[2] = " << b[2] << std::endl;
    std::cout << "2[b] = " << 2 [b] << std::endl;

    delete[] b;

    std::cout << " Необработанный указатель " << std::endl;
    Entity *e0 = new Entity(" E0");
    delete e0;

    std::cout << " Необработанный указатель на объект в стеке в области видимости"  << std::endl;
    Entity *e1;
    {
        Entity e2 = Entity(" E2");
        e1 = &e2;
        std::cout << e1->getName() << std::endl;
    }
    std::cout << e1->getName() << std::endl;

    std::cout << " Необработанный указатель в области видимости " << std::endl;
    {
        Entity *e3 = new Entity(" E3");
    }
    std::cout << " E3 все еще не удален " << std::endl;

    std::cout << " Умный unique_ptr " << std::endl;
    {
        std::unique_ptr<Entity> e4 = std::make_unique<Entity>(" E4");
    }

    std::cout << " Умный shared_ptr, E6 ссылки E5 " << std::endl;
    std::shared_ptr<Entity> e6;
    {
        auto e5 = std::make_shared<Entity>(" E5");
        e6 = e5;
    }
    std::cout << " E5 все еще существует при переменной E6 " << std::endl;
    e6.reset();

    std::cout << " Умный weak_ptr " << std::endl;
    std::weak_ptr<Entity> e7;
    {
        auto e8 = std::make_shared<Entity>(" E8");
        e7 = e8;
    }
    std::cout << (e7.expired() ? " Е7 истекший ":" Е7 все еще существует ") << std::endl;
}
